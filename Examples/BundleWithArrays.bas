REM *** Bundle Package Test Program *** / A_BundleWithArrays01.bas

FN.DEF PutBundleContens(globals)

 b$ = "B"
 ARRAY.LOAD c$[], "D","E","F","G","H","I"

 BUNDLE.PUT globals,"myString_A", "A"
 BUNDLE.PUT globals,"myString_B", c$[]
 BUNDLE.PUT globals,"myString_B", b$
 BUNDLE.PUT globals,"myStringArray_C", c$[]

 l = 97
 ARRAY.LOAD m[], 4,-5,6,-7,8,-9

 BUNDLE.PUT globals,"myDouble_K", -47.11
 BUNDLE.PUT globals,"myDouble_L", l
 BUNDLE.PUT globals,"myDoubleArray_M", m[]
 DIM Z[2,3] 
 Z[1,1] = 1
 Z[1,2] = 2
 Z[1,3] = 3
 Z[2,1] = 4
 Z[2,2] = 5
 Z[2,3] = 6
 BUNDLE.PUT globals,"myDoubleArray_Z", Z[]

 FN.RTN 1

FN.END

DEBUG.ON
BUNDLE.CREATE globals
PutBundleContens(globals)

BUNDLE.GET globals,"myString_A", myString_A$
BUNDLE.GET globals,"myString_B", myString_B$
DIM myStringArray_C$[11]
BUNDLE.GET globals,"myStringArray_C", myStringArray_C$[]

PRINT "myString_A", myString_A$
PRINT "myString_B", myString_B$
PRINT "myStringArray_C", myStringArray_C$[4]
BUNDLE.TYPE globals,"myString_B", varType$
PRINT "Type myString_B", varType$
BUNDLE.TYPE globals,"myStringArray_C", varType$
PRINT "Type myStringArray_C", varType$

PRINT 
BUNDLE.GET globals,"myDouble_K", myDouble_K
BUNDLE.GET globals,"myDouble_L", myDouble_L
!	DIM myDouble_M[11]
ARRAY.LOAD myDouble_M[],1,2,3,4,5,6,7
!	DIM myDouble_Z[2,3]
BUNDLE.GET globals,"myDoubleArray_M", myDouble_M[]
BUNDLE.GET globals,"myDoubleArray_Z", myDouble_Z[]
PRINT "myDouble_K", myDouble_K
PRINT "myDouble_L", myDouble_L

PRINT "myDoubleArray_M", myDouble_M[3]
PRINT "myDoubleArray_Z", myDouble_Z[2,1]

BUNDLE.TYPE globals,"myDoubleArray_M", varType$
PRINT "Type myDoubleArray_M", varType$
BUNDLE.TYPE globals,"myDouble_K", varType$
PRINT "Type myDouble_K", varType$
BUNDLE.TYPE globals,"myDoubleArray_Z", varType$
PRINT "Type myDoubleArray_Z", varType$

DIM Z[2,2] 
Z[1,1] = 1
Z[1,2] = 2
Z[2,1] = 3
Z[2,2] = 4

PRINT "Array.copy, copy and dispose to one dimension"
ARRAY.COPY Z[], W[]
! PRINT W[2,2] throws an error ?
PRINT W[4] % Print 4
! The manual should be teach, that this command is developed for arrays with one dimension.
! Arrays with more dimensions will be dispose to one dimension.
PRINT "Array copy with BUNDLE.PUT and BUNDLE.GET"
BUNDLE.PUT globals,"Z[]", Z[]
BUNDLE.GET globals,"Z[]", W[]
PRINT W[2,2]

BUNDLE.KEYS globals, list_ptr
LIST.SIZE list_ptr, lSize
PRINT "Print Bundle.Keys,   Keys: ",lSize
FOR I = 1 TO lSize
 LIST.GET list_ptr, I, contens$
 PRINT contens$
NEXT I

! BUNDLE.CREATE myFirstBundle % In this case obsolet, because BUNDLE Auto-Create
BUNDLE.PB myFirstBundle, "globals", globals
BUNDLE.KEYS myFirstBundle, lp
LIST.SIZE lp, listSize
PRINT "keys myFirstBundle listSize: ", listSize
FOR i = 1 TO listSize
 LIST.GET lp, i, text$
 PRINT text$
NEXT I
BUNDLE.GB myFirstBundle, "globals", globals
BUNDLE.PB myFirstBundle, "globals", globals
BUNDLE.CREATE mySecondBundle
BUNDLE.GB myFirstBundle, "globals", mySecondBundle
BUNDLE.KEYS mySecondBundle, lp
LIST.SIZE lp, listSize
PRINT "keys mySecondBundle listSize: ", listSize
FOR i = 1 TO listSize
 LIST.GET lp, i, text$
 PRINT text$
NEXT I

BUNDLE.GET mySecondBundle,"myString_B", myString_B_In_Second_Bundle$
PRINT "myString_B_In_Second_Bundle$", myString_B_In_Second_Bundle$
BUNDLE.PL myFirstBundle, "globals_list_ptr", list_ptr
BUNDLE.KEYS myFirstBundle, lp
LIST.SIZE lp, listSize
PRINT "keys myFirstBundle listSize: ", listSize
FOR i = 1 TO listSize
 LIST.GET lp, i, text$
 PRINT text$
NEXT I
LIST.CREATE S, list_ptr2
BUNDLE.GL myFirstBundle, "globals_list_ptr", list_ptr2
LIST.SIZE list_ptr2, listSize
PRINT "keys list_ptr2 listSize: ", listSize
FOR i = 1 TO listSize
 LIST.GET list_ptr2, i, text$
 PRINT text$
NEXT I
STACK.CREATE S, stack_ptr
STACK.PUSH stack_ptr, "A"
STACK.PUSH stack_ptr, "B"
STACK.PUSH stack_ptr, "C"
BUNDLE.PS myFirstBundle, "stack_ptr", stack_ptr
STACK.CREATE S, stack_ptr2
BUNDLE.GS myFirstBundle, "stack_ptr", stack_ptr2
STACK.ISEMPTY stack_ptr2, stackIsE
IF stackIsE < 1
 DO
  STACK.POP stack_ptr2, stackText$
  STACK.ISEMPTY stack_ptr2, stackIsE
  PRINT "stackText$: ", stackText$
 UNTIL stackIsE > 0
ENDIF

STACK.CREATE N, stack_ptr3
STACK.PUSH stack_ptr3, 1
STACK.PUSH stack_ptr3, 2
STACK.PUSH stack_ptr3, 3
BUNDLE.PS myFirstBundle, "stack_ptr2", stack_ptr3
STACK.CREATE N, stack_ptr4
BUNDLE.GS myFirstBundle, "stack_ptr2", stack_ptr4
STACK.ISEMPTY stack_ptr4, stackIsE
IF stackIsE < 1
 DO
  STACK.POP stack_ptr4, stackNum
  STACK.ISEMPTY stack_ptr4, stackIsE
  PRINT "stackText$: ", stackNum
 UNTIL stackIsE > 0
ENDIF

BUNDLE.TYPE myFirstBundle, "globals", bType$
PRINT "Bundle Tag Type: globals ", bType$
BUNDLE.TYPE myFirstBundle, "globals_list_ptr", bType$
PRINT "Bundle Tag Type: globals_list_ptr ", bType$
BUNDLE.TYPE myFirstBundle, "stack_ptr", bType$
PRINT "Bundle Tag Type: stack_ptr ", bType$
BUNDLE.TYPE myFirstBundle, "stack_ptr2", bType$
PRINT "Bundle Tag Type: stack_ptr2 ", bType$
BUNDLE.COPY myFirstBundle, b2
BUNDLE.TYPE b2, "stack_ptr2", b2Type$
PRINT "Bundle Tag Type: stack_ptr2 ", b2Type$

END
