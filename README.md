# OliBasic is a fork of RFO-Basic 

Current Version XXIV 2018-07-20

**This project has the package id "com.rfo.basicTest".**

Copyrights (C) 2010 - 2017 of the base code and 
licensing under the terms of GNU GPLv3 by Paul Laughton.

Copyrights  (C) 2016 - 2018 for all changes and the whole 
composition by Gregor Tiemeyer.
Licensed under the terms of GNU GPLv3

Special thanks to:
* Paul Laughton the creator of BASIC! for Android
* Marc Sammartanos the man who realize a lot of milestones and who kept all stuff together.
* Nicolas Mougino the creator of the BASIC!-Compiler for Android and a lot other tools.


[CHANGES](https://gitlab.com/OliBasic/Main/blob/master/Changes.md)

[HOMEPAGE](https://OliBasic.gitlab.io/About/index.html)


